<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserLoginRequest;
use App\Models\User;
use App\Repositories\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use \Carbon\Carbon;

class AuthApiController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
        $this->userModel = new Repository($user);
    }

    /**
     * Authenticate User using credentials
     *
     * @param {Request} request
     * @return  response {*} JWT
     */
    public function authenticate(UserLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            return response([
                'status' => 'error',
                'error' => 'invalid.credentials',
                'message' => 'Invalid Credentials.',
            ], 400);
        }

        return response([
            'status' => 'success',
            'token' => $token,
        ]);

    }

    /**
     * End Section
     * Invalidate JWT and update user field last_login
     *
     */
    public function logout(Request $request)
    {
        $authUser = $this->userModel->show($request->user()->id);
        $authUser->last_login = new Carbon();
        JWTAuth::invalidate(); // Invalidate user token
        $authUser->save(); // Save user last_login field
        return response([
            'status' => 'success',
            'messge' => 'Logged out Successfully.',
        ], 200);
    }

    /**
     * Get Auth User profile
     * @param Request $request
     * @return Response response
     */
    public function profile(Request $request)
    {
        $token = $request->token;
        $user = JWTAuth::authenticate($token);

        return response()->json(compact('user'));

    }

    /**
     * Get Auth user roles
     * @param Request $request
     * @return Response response
     */
    public function roles(Request $request)
    {
        $token = $request->token;
        $roles = $request->user()->roles()->get();

        return response()->json(compact('roles'));

    }
}
