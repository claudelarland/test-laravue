<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\Models\User;
use App\Repositories\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function __construct(User $user)
    {
        $this->middleware('is:administrator');
        $this->userModel = new Repository($user);

    }

    public function index(Request $request)
    {
        $users = User::orderBy('created_at', 'desc')
            ->paginate($request->input('size') ?? config('global.itemsPerPage'));

        if ($request->input('text')) {
            $users = User::where('first_name', 'like', "%{$request->text}%")
                ->orWhere('last_name', 'like', "%{$request->text}%")->orderBy('created_at', 'desc')
                ->paginate($request->input('size') ?? config('global.itemsPerPage'));
        }
        return response()->json(compact('users'));
    }

    //Retrieve all users
    public function get(Request $request)
    {
        $adminId = Role::whereSlug('administrator')->first()->id;
        $users = User::whereHas('roles', function ($query) use ($adminId) {
            $query->where('role_id', '<>', $adminId);
        })->get();

        return response()->json(compact('users'));
    }

    /**
     * Store new user in database
     */
    public function store(CreateUserRequest $request)
    {
        DB::beginTransaction(); // begin mysql transaction
        try {
            $data = $request->only(['last_name', 'first_name', 'email', 'phone', 'password']);
            $data['avatar'] = "https: //picsum.photos/200/300/?random";
            $created = $this->userModel->create($data);

            // Attach Member role to the created user
            $role = Role::whereSlug('member')->first();
            $created->attachRole($role);
            DB::commit(); // Commit the transaction
            return response([
                'status' => 'success',
                'message' => "created",
            ], 201);

        } catch (Throwable $ex) {
            DB::rollback(); // rollback if err

            return response([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ], 502);

        }
    }

    /**
     * Update user
     */
    public function update(UpdateUserRequest $request)
    {
        try {
            $data = $request->only(['last_name', 'first_name', 'email', 'phone']);
            $this->userModel->update($data, $request->input('id'));

            return response([
                'status' => 'success',
                'message' => "updated",
            ], 200);

        } catch (Exception $ex) {
            return response([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ], 500);

        }
    }

    /**
     * Remove user
     */
    public function remove(Request $request)
    {
        try {

            $this->userModel->destroy($request->id);
            return response([
                'status' => 'success',
                'message' => "deleted",
            ], 200);

        } catch (Exception $ex) {
            return response([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ], 500);

        }
    }

}
