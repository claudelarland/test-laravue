<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateInterviewRequest;
use App\Http\Requests\UpdateInterviewRequest;
use App\Models\Interview;
use App\Models\User;
use App\Repositories\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InterviewController extends Controller
{
    public function __construct(Interview $interview, User $user)
    {
        $this->middleware('is:administrator');
        $this->interviewModel = new Repository($interview);
        $this->userModel = new Repository($user);

    }

    /**
     * Get interviews
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function index(Request $request)
    {
        $interviews = Interview::with('users')->orderBy('created_at', 'desc')
            ->paginate($request->input('size') ?? config('global.itemsPerPage'));

        if ($request->input('text')) {
            $interviews = Interview::where('title', 'like', "%{$request->text}%")
                ->orWhere('subject', 'like', "%{$request->text}%")
                ->orWhere('address', 'like', "%{$request->text}%")
                ->orderBy('created_at', 'desc')
                ->paginate($request->input('size') ?? config('global.itemsPerPage'));
        }

        return response()->json(compact('interviews'));
    }

    /**
     * Store new Interview
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateInterviewRequest $request)
    {
        try {
            DB::beginTransaction();
            $data = $request->only(['title', 'address', 'description', 'subject']);
            $data['slug'] = str_slug($data['title']);
            $created = $this->interviewModel->create($data);

            // $users = collect($request->input('selected'))->map(function ($item) {
            //     return $this->userModel->show($item);
            // });
            //$created->users()->saveMany($users);
            $created->users()->sync($request->input('selected'));

            DB::commit();
            return response([
                'status' => 'success',
                'message' => "created",
            ], 201);

        } catch (Throwable $ex) {
            DB::rollback();
            return response([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ], 502);

        }
    }

    /**
     * Update Interview and related users
     * @param App\Http\Requests\UpdateInterviewRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateInterviewRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->only(['title', 'address', 'description', 'subject']);
            $this->interviewModel->update($data, $request->input('id'));
            $updated = $this->interviewModel->show($request->input('id'));
            $updated->users()->sync($request->input('selected'));
            DB::commit();

            return response([
                'status' => 'success',
                'message' => "updated",
            ], 200);

        } catch (Exception $ex) {
            DB::rollback();

            return response([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ], 500);

        }
    }

    /**
     * Remove Interview
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function remove(Request $request)
    {
        try {

            $this->interviewModel->destroy($request->id);
            return response([
                'status' => 'success',
                'message' => "deleted",
            ], 200);

        } catch (Exception $ex) {
            return response([
                'status' => 'error',
                'message' => $ex->getMessage(),
            ], 500);

        }
    }
}
