<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        //$this->middleware('jwt.auth');

        if (!$request->user()->hasRole($role)) {
            abort(401, 'You dont have permission to perform this action.');
        }

        return $next($request);
    }
}
