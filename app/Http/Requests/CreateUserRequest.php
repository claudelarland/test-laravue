<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:users|email',
            'phone' => 'required|regex:/^[+]\d/i',
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone.regex' => 'Enter a valid phone number (Ex : +23333333)',
        ];
    }
}
