<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|regex:/^[+]\d/i',
            'first_name' => 'required',
            'id' => 'required',
            'email' => 'required|email',
            'last_name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone.regex' => 'Enter a valid phone number (Ex : +23333333)',
        ];
    }
}
