<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{

    protected $fillable = ['title', 'address', 'subject', 'description', 'slug'];

    /**
     * User and Interview RelationShip
     *
     * User can belongs to Many Interview
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}
