<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name', 'slug', 'description'];

    /**
     * User and Team RelationShip
     *
     * Team can belongs to users
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
