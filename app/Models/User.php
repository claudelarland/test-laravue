<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'password', 'avatar', 'phone', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Automatically hash the password.
     *
     * @param $password
     */
    protected function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * User and Role relationship
     *
     * User belongs to many roles
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user')->withTimestamps();
    }

    /**
     * Attch a role to user
     *
     * @params {Role} $role
     *
     */
    public function attachRole($role)
    {
        return $this->roles()->attach($role);
    }

    /**
     * User and Interview RelationShip
     *
     * User can belongs to Many Interview
     */
    public function interview()
    {
        return $this->belongsToMany(Interview::class)->withTimestamps();

    }

    /**
     * User and Role relationship
     *
     * User belongs to many roles
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class)->withTimestamps();
    }

    public function hasRole($role)
    {
        return null !== $this->roles->where('slug', $role)->first();

    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
