<?php

use App\Models\Role;
use App\Models\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use \Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $member = Role::where('slug', 'member')->first();

        $users = [
            [
                'last_name' => 'AKPASSOU',
                'first_name' => 'Arland Claudel',
                'email' => 'aakpassou@yahoo.fr',
                'phone' => '+212(0)605735191',
                'password' => 'kaokeb@pp2018',
                'avatar' => 'https://picsum.photos/200/300/?random',
                'role' => ['administrator'],
            ],
            [
                'last_name' => 'Pierre',
                'first_name' => 'Moreau',
                'email' => 'cto@kaokeb.com',
                'phone' => '+212 (0)673773773',
                'password' => 'kaokeb@pp2018',
                'avatar' => 'https://picsum.photos/200/300/?random',
                'role' => ['administrator'],
            ],
        ];

        foreach ($users as $key => $user) {
            $createdUser = User::create([
                'last_name' => $user['last_name'],
                'first_name' => $user['first_name'],
                'email' => $user['email'],
                'phone' => $user['phone'],
                'avatar' => $user['avatar'],
                'status' => 1,
                'password' => $user['password'],
            ]);

            // Attach roles too user
            foreach ($user['role'] as $key => $role) {
                $userRole = Role::where('slug', $role)->first();
                $createdUser->attachRole($userRole);
            }

        }

        $faker = Faker::create();
        foreach (range(1, 15) as $index) {
            $created = User::create([
                'last_name' => $faker->lastName,
                'first_name' => $faker->firstName,
                'email' => $faker->email,
                'phone' => $faker->e164PhoneNumber,
                'avatar' => $faker->imageUrl($width = 640, $height = 480),
                'status' => 1,
                'password' => bcrypt('secret'),
                'created_at' => Carbon::now(),
            ]);

            $created->attachRole($member);

        }

    }
}
