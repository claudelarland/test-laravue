<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [
                'name' => 'Membre',
                'slug' => 'member',
                'description' => 'Simple utilisateur de la plateforme',
            ], [
                'name' => 'Administrateur',
                'slug' => 'administrator',
                'description' => 'Administarteur peut gérer tous les autres roles sauf la création des administrateurs',
            ],

        ]);
    }
}
