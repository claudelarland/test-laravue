# Laravel - VueJs App

Using Laravel and vuejs 

## Getting Started

### Prerequisites

Create a MySql database named  'kaokebapp' with collation 'utf8mb4_general_ci'

### Installing

Install Php dependencies 

```
composer install
```
Install VueJs Dependencies

```
npm run setup
```

### Laravel 

*Check .env file to make sure that database variables are corrects (HOST-PORT-USERNAME-PASSWORD) for MySql connection 

*Migrate and seed some users 

```
php artisan migrate --seed
```

If you use Laravel valet,skip this command 

```
php artisan serve
```

### Start Front (VueJs)

Make this command to start vue

```
npm run watch 
```


YOU CAN CONNECT TO YOUR APP USING ONE OF TWO ADMIN AS SPECIFIED IN ./database/seeds/UsersTableSeeder | Users Array