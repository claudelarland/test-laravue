<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class AuthTest extends TestCase
{

    /**
     * Test login with the right credentials
     *
     * @return void
     */

    public function test_login_with_right_credentials()
    {
        $user = factory(User::class)->create();

        $response = $this->json('POST', '/api/v1/auth/login', ['email' => $user['email'], 'password' => 'test_use']);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'status', 'token',
            ]);

    }

    /**
     * Test login with the wrong credentials
     *
     * Error (400) Bad Credentials
     * @return void
     */
    public function test_login_with_invalid_credentials(Type $var = null)
    {
        $response = $this->json('POST', '/api/v1/auth/login', ['email' => 'aakpassou@yahoo.fr', 'password' => 'test123']);

        $response
            ->assertStatus(400)
            ->assertJsonStructure([
                'status', 'error',
            ]);

    }

    /**
     * Test login with the wrong credentials
     *
     * Error (400) Bad Credentials
     * @return void
     */
    public function test_login_with_empty_credentials_value(Type $var = null)
    {
        $response = $this->json('POST', '/api/v1/auth/login', ['email' => '', 'password' => '']);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors([
                'email', 'password',
            ]);

    }
}
