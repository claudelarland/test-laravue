const mix = require('laravel-mix');
let LiveReloadPlugin = require('webpack-livereload-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
mix.webpackConfig({
    plugins: [new LiveReloadPlugin()]
});

function resolve(dir) {
    return path.join(__dirname, 'resources/js/frontend', dir)
}

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /.svg$/,
                loader: 'vue-svg-loader',
                include: [resolve('src/icons/svg/*.svg')],

                options: {
                    options: {
                        symbolId: 'icon-[name]'
                    }
                }
            },
        ]
    },
    resolve: {
        extensions: [
            '.js', '.vue', '.json', '.svg'
        ],
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': __dirname + '/resources/js/frontend/src',
            'jquery': 'jquery/src/jquery.js'
        }
    }
})