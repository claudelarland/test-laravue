<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {

    Route::post('auth/login', 'Auth\AuthApiController@authenticate');
    Route::post('auth/logout', 'Auth\AuthApiController@logout');
    Route::get('auth/profile', 'Auth\AuthApiController@profile');
    Route::get('auth/roles', 'Auth\AuthApiController@roles');

    Route::group(['middleware' => 'jwt.auth'], function () {

        // User Route Actions
        Route::group(['prefix' => 'users'], function () {
            Route::post('get', 'Admin\UserController@index');
            Route::get('all', 'Admin\UserController@get');
            Route::post('add', 'Admin\UserController@store');
            Route::put('update', 'Admin\UserController@update');
            Route::delete('remove/{id}', 'Admin\UserController@remove');
        });

        // Interviews Route Actions
        Route::group(['prefix' => 'interviews'], function () {
            Route::post('get', 'Admin\InterviewController@index');
            Route::post('add', 'Admin\InterviewController@store');
            Route::put('update', 'Admin\InterviewController@update');
            Route::delete('remove/{id}', 'Admin\InterviewController@remove');
        });

    });
});
